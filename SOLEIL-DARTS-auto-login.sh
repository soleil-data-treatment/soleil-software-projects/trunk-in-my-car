#!/bin/sh

# create local copy of script in /tmp
# r=`realpath $0`
# cp $r /tmp

# wait for network and LDAP to be ready
sleep 5

# get information from the LDAP: user exists ?

LDAP=ldap://195.221.10.1
s=`ldapsearch -H $LDAP -x -b "dc=EXP" -s sub "mail=@USER@"`

t=`echo $s | grep -o numEntries`
if [ -z $t ]; then
  s=`ldapsearch -H $LDAP -x -b "dc=EXP" -s sub "cn=@USER@"`
fi

t=`echo $s | grep -o numEntries`
if [ -z $t ]; then
  s=`ldapsearch -H $LDAP -x -b "dc=EXP" -s sub "uid=@USER@"`
fi

echo "user=@USER@" > /tmp/user.txt

t=`echo $s | grep -o "loginShell: .* .*" | awk '{ print $2 }'`
if [ x$t = 'x/dev/null' ]; then
  can_login=0;
else
  can_login=1;
  echo "canLogin=$can_login" >> /tmp/user.txt
fi

# get email
t=`echo $s | grep -o "mail: .*" | awk '{ print $2 }'`
email=$t
c=`echo $t | grep -o "@"`
if [ -z $c ]; then
  has_email=0;
else
  has_email=1;
  echo "email=$email" >> /tmp/user.txt
fi

# get uid
t=`echo $s | grep -o "uidNumber: .*" | awk '{ print $2 }'`
uid=$t
echo "uidNumber=$uid" >> /tmp/user.txt

# get gid
t=`echo $s | grep -o "gidNumber: .*" | awk '{ print $2 }'`
gid=$t
echo "gidNumber=$gid" >> /tmp/user.txt

# get homedir
t=`echo $s | grep -o "homeDirectory: .*" | awk '{ print $2 }'`
homedir=$t
echo "homeDirectory=$homedir" >> /tmp/user.txt

cat /tmp/user.txt

# switch auto-login to user if can_login and has_email, else remain as guest
# if [ $can_login = 1 -a $has_email = 1 ]; then
if [ $can_login = 1 ]; then
# map /home to the one on host (allow Jupyter interaction)
  mkdir /mnt/home
  mount -t 9p -o trans=virtio,access=client host_home /mnt/home

  # make sure account exists, from LDAP
  echo "Creating user @USER@"
  # create a user with proper uid/gid and local account (for NFS) ?
  pass=$(perl -e 'print crypt($ARGV[0], "@PW@")' $password)
  addgroup --gid $gid group_@USER@
  
  # create an alias to the persistent area /mnt/$homedir in $homedir
  useradd -m -p "$pass" -d $homedir @USER@ -s /usr/bin/bash -u $uid -g $gid
  
  echo "Setting background"
  su -c 'gsettings set org.cinnamon.desktop.background picture-uri "file:///usr/share/backgrounds/desktop-background-SOLEIL.png"; exit' @USER@
  su -c "gsettings set org.gnome.libgnomekbd.keyboard layouts \"['fr','fr\tmac','us','us\tmac']\"; exit" @USER@
  su -c "gsettings set org.cinnamon.desktop.interface toolkit-accessibility true; exit" @USER@
  su -c "gsettings set org.cinnamon.desktop.a11y.keyboard enable true; exit" @USER@
  su -c "gsettings set org.gnome.desktop.a11y.applications screen-keyboard-enabled true; exit" @USER@
  su -c "gsettings set org.cinnamon enabled-applets \"['panel1:left:0:menu@cinnamon.org:0', 'panel1:left:1:show-desktop@cinnamon.org:1', 'panel1:left:2:grouped-window-list@cinnamon.org:2', 'panel1:right:0:systray@cinnamon.org:3', 'panel1:right:1:xapp-status@cinnamon.org:4', 'panel1:right:2:notifications@cinnamon.org:5', 'panel1:right:3:printers@cinnamon.org:6', 'panel1:right:4:removable-drives@cinnamon.org:7', 'panel1:right:6:favorites@cinnamon.org:9', 'panel1:right:7:network@cinnamon.org:10', 'panel1:right:8:sound@cinnamon.org:11', 'panel1:right:9:power@cinnamon.org:12', 'panel1:right:10:calendar@cinnamon.org:13', 'panel1:right:11:a11y@cinnamon.org:14', 'panel1:right:12:keyboard@cinnamon.org:15', 'panel1:right:14:trash@cinnamon.org:17']\"" @USER@
  
  # mount raw data and JupyterHub persistent area
  if [ -d "/mnt/$homedir" ]; then
    echo "Add $homedir/Desktop/jupyterhub_area"
    su -c "ln -s /mnt/$homedir $homedir/Desktop/jupyterhub_area; exit" @USER@
  fi
  if [ -d "/nfs/ruche" ]; then
    echo "Add $homedir/Desktop/ruche"
    su -c "ln -s /nfs/ruche $homedir/Desktop/ruche; exit" @USER@
  fi
  RUCHEHOME=`echo "$homedir" | sed -e 's|/home|/nfs/ruche|g'`
  if [ -d "$RUCHEHOME" ]; then
    echo "Add $homedir/Desktop/home_ruche"
    su -c "ln -s $RUCHEHOME $homedir/Desktop/home_ruche; exit" @USER@
  fi
  
  # set auto-login
  # take into account the new auto-login with restart
  echo "Setting auto-login"
  
  s=`systemctl status lightdm | grep -o Active`
  if [ -z "$s" ]; then
    echo "[daemon]"                   > /etc/gdm3/custom.conf
    echo "AutomaticLoginEnable=true" >> /etc/gdm3/custom.conf
    echo "AutomaticLogin=@USER@"     >> /etc/gdm3/custom.conf
    echo "TimedLoginEnable=true"     >> /etc/gdm3/custom.conf
    echo "TimedLogin=@USER@"         >> /etc/gdm3/custom.conf
    echo "TimedLoginDelay=0"         >> /etc/gdm3/custom.conf
    sleep 1
    systemctl restart gdm3.service
  else
    if [ -d "/etc/lightdm/lightdm.conf.d" ]; then
      FILE=/etc/lightdm/lightdm.conf.d/12-autologin.conf
      echo "[SeatDefaults]"          > $FILE
    else
      FILE=/etc/lightdm/lightdm.conf
      echo "[SeatDefaults]"         >> $FILE
    fi
    
    echo "autologin-user=@USER@"    >> $FILE
    echo "autologin-user-timeout=0" >> $FILE
    sleep 1
    systemctl restart lightdm.service
  fi
  
fi
