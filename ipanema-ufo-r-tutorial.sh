#!/bin/sh

# create local copy of script in /tmp
r=`realpath $0`
cp $r /tmp

# wait for the network to come
sleep 60

# SOLEIL proxy
export http_proxy=http://195.221.10.6:8080
export https_proxy=http://195.221.10.6:8080
export ftp_proxy=http://195.221.10.6:8080

# get deb packages from Debian Salsa CI
cd /tmp
wget https://salsa.debian.org/science-team/ufo-filters/-/jobs/2209939/artifacts/raw/debian/output/ufo-filters-data_0.16.0.168.gd10a7c5+dfsg1-1+salsaci_all.deb
wget https://salsa.debian.org/science-team/ufo-filters/-/jobs/2209939/artifacts/raw/debian/output/ufo-filters-doc_0.16.0.168.gd10a7c5+dfsg1-1+salsaci_all.deb
wget https://salsa.debian.org/science-team/ufo-filters/-/jobs/2209939/artifacts/raw/debian/output/ufo-filters_0.16.0.168.gd10a7c5+dfsg1-1+salsaci_amd64.deb

# install the packages
apt install ./ufo*.deb

rm ./ufo*.deb
echo "UFO-filters upgrade: Done"

# now can use e.g. 
#   https://plmlab.math.cnrs.fr/tomogroup/reco-form
# for tomography reconstruction using R
# requires CRAN OpenCL and r4tomo

# Done
